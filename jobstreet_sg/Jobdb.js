import { gql,GraphQLClient } from 'graphql-request'
import fs from 'fs'

const country = "ph"
const jobFunction = 508
const endpoint =`https://xapi.supercharge-srp.co/job-search/graphql?country=${country}&isSmartSearch=false`;
const client = new GraphQLClient(endpoint)

const query = gql`
query getJobs($country: String, $locale: String, $keyword: String, $createdAt: String, $jobFunctions: [Int], $categories: [String], $locations: [Int], $careerLevels: [Int], $minSalary: Int, $maxSalary: Int, $salaryType: Int, $candidateSalary: Int, $candidateSalaryCurrency: String, $datePosted: Int, $jobTypes: [Int], $workTypes: [String], $industries: [Int], $page: Int, $pageSize: Int, $companyId: String, $advertiserId: String, $userAgent: String, $accNums: Int, $subAccount: Int, $minEdu: Int, $maxEdu: Int, $edus: [Int], $minExp: Int, $maxExp: Int, $seo: String, $searchFields: String, $candidateId: ID, $isDesktop: Boolean, $isCompanySearch: Boolean, $sort: String, $sVi: String, $duplicates: String, $flight: String, $solVisitorId: String) {
    jobs(
      country: $country
      locale: $locale
      keyword: $keyword
      createdAt: $createdAt
      jobFunctions: $jobFunctions
      categories: $categories
      locations: $locations
      careerLevels: $careerLevels
      minSalary: $minSalary
      maxSalary: $maxSalary
      salaryType: $salaryType
      candidateSalary: $candidateSalary
      candidateSalaryCurrency: $candidateSalaryCurrency
      datePosted: $datePosted
      jobTypes: $jobTypes
      workTypes: $workTypes
      industries: $industries
      page: $page
      pageSize: $pageSize
      companyId: $companyId
      advertiserId: $advertiserId
      userAgent: $userAgent
      accNums: $accNums
      subAccount: $subAccount
      minEdu: $minEdu
      edus: $edus
      maxEdu: $maxEdu
      minExp: $minExp
      maxExp: $maxExp
      seo: $seo
      searchFields: $searchFields
      candidateId: $candidateId
      isDesktop: $isDesktop
      isCompanySearch: $isCompanySearch
      sort: $sort
      sVi: $sVi
      duplicates: $duplicates
      flight: $flight
      solVisitorId: $solVisitorId
    ) {
      total
      totalJobs
      relatedSearchKeywords {
        keywords
        type
        totalJobs
      }
      solMetadata
      suggestedEmployer {
        name
        totalJobs
      }
      queryParameters {
        key
        searchFields
        pageSize
      }
      experiments {
        flight
      }
      jobs {
        id
        sellingPoints
      }
    }
  }
`

var variables  = {
  "keyword": "",
  "jobFunctions": [
    jobFunction
  ],
  "locations": [],
  "salaryType": 1,
  "jobTypes": [],
  "createdAt": null,
  "careerLevels": [],
  "page": 1,
  "country": country,
  "sVi": "",
  "solVisitorId": "1a9219b7-e948-4d25-bffb-ddbec839ab82",
  "categories": [
    jobFunction+""
  ],
  "workTypes": [],
  "userAgent": "Mozilla/5.0%20(X11;%20Linux%20x86_64)%20AppleWebKit/537.36%20(KHTML,%20like%20Gecko)%20Chrome/103.0.5060.114%20Safari/537.36%20Edg/103.0.1264.51",
  "industries": [],
  "locale": "en"
}

const getJobsDetails = async (id) => {
  const query = `query getJobDetail($jobId: String, $locale: String, $country: String, $candidateId: ID, $solVisitorId: String, $flight: String) {
    jobDetail(
      jobId: $jobId
      locale: $locale
      country: $country
      candidateId: $candidateId
      solVisitorId: $solVisitorId
      flight: $flight
    ) {
      header {
        jobTitle
        company {
          name
        }
      }
      jobDetail {
        summary
        jobDescription {
          html
        }
        jobRequirement {
          careerLevel
          yearsOfExperience
          qualification
          fieldOfStudy
          industryValue {
            value
            label
          }
          skills
          employmentType
          languages
          postedDate
          closingDate
          jobFunctionValue {
            name
          }
        }
      }
    }
  }`

const variables = {
  "jobId": id,
  "country": country,
  "locale": "en",
  "candidateId": "",
  "solVisitorId": "1a9219b7-e948-4d25-bffb-ddbec839ab82"
}
  const data = await client.request(query, variables);
  return data;
}

  var result_job =[] 
  var total
  while(true)  {
    const data = await client.request(query, variables);
    if (variables.page == 1) {
        total = data.jobs.total
    }

    if (data.jobs.jobs.length == 0) {
        break;
    }else{
        variables.page += 1;
        data.jobs.jobs.forEach(async data => {
          const objTemplate = {
            companyName: "",
            jobTitle: "",
            jobSummary: [],
            jobDescription: "",
            sellingPoints: data.sellingPoints,
            jobFunction: []
          }
          const result = await getJobsDetails(data.id)
          try {
          objTemplate.jobTitle = result.jobDetail.header.jobTitle
          objTemplate.companyName = result.jobDetail.header.company.name
          objTemplate.jobSummary = result.jobDetail.jobDetail.summary
          objTemplate.jobDescription = result.jobDetail.jobDetail.jobDescription.html
          objTemplate.jobFunction = result.jobDetail.jobDetail.jobRequirement.jobFunctionValue.map(row => {
            return row.name
          })
          result_job.push(objTemplate);
        } catch (error) {
            console.log(result);
        }
        });
        console.log(`Result Data : ${result_job.length}/${total}`);
    }
  }

  fs.writeFileSync(`./jobstreet_sg/result_data/${Date.now()}_${country}.json`,JSON.stringify(result_job, null, 2))
  console.log("Successful Export Data");