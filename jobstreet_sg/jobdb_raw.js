import { gql,GraphQLClient } from 'graphql-request'
import fs from 'fs'

const country = "ph"
const jobFunction = 508
const endpoint =`https://xapi.supercharge-srp.co/job-search/graphql?country=${country}&isSmartSearch=false`;
const client = new GraphQLClient(endpoint)

const getJobsDetails = async (id) => {
  const query = `query getJobDetail($jobId: String, $locale: String, $country: String, $candidateId: ID, $solVisitorId: String, $flight: String) {
    jobDetail(
      jobId: $jobId
      locale: $locale
      country: $country
      candidateId: $candidateId
      solVisitorId: $solVisitorId
      flight: $flight
    ) {
      id
      pageUrl
      jobTitleSlug
      applyUrl {
        url
        isExternal
      }
      isExpired
      isConfidential
      isClassified
      accountNum
      advertisementId
      subAccount
      showMoreJobs
      adType
      header {
        banner {
          bannerUrls {
            large
          }
        }
        salary {
          max
          min
          type
          extraInfo
          currency
          isVisible
        }
        logoUrls {
          small
          medium
          large
          normal
        }
        jobTitle
        company {
          name
          url
          slug
          advertiserId
        }
        review {
          rating
          numberOfReviewer
        }
        expiration
        postedDate
        postedAt
        isInternship
      }
      companyDetail {
        companyWebsite
        companySnapshot {
          avgProcessTime
          registrationNo
          employmentAgencyPersonnelNumber
          employmentAgencyNumber
          telephoneNumber
          workingHours
          website
          facebook
          size
          dressCode
          nearbyLocations
        }
        companyOverview {
          html
        }
        videoUrl
        companyPhotos {
          caption
          url
        }
      }
      jobDetail {
        summary
        jobDescription {
          html
        }
        jobRequirement {
          careerLevel
          yearsOfExperience
          qualification
          fieldOfStudy
          industryValue {
            value
            label
          }
          skills
          employmentType
          languages
          postedDate
          closingDate
          jobFunctionValue {
            code
            name
            children {
              code
              name
            }
          }
          benefits
        }
        whyJoinUs
      }
      location {
        location
        locationId
        omnitureLocationId
      }
      sourceCountry
    }
  }`

const variables = {
  "jobId": id,
  "country": country,
  "locale": "en",
  "candidateId": "",
  "solVisitorId": "1a9219b7-e948-4d25-bffb-ddbec839ab82"
}
  const data = await client.request(query, variables);
  return data;
}
const query = gql`query getJobs($country: String, $locale: String, $keyword: String, $createdAt: String, $jobFunctions: [Int], $categories: [String], $locations: [Int], $careerLevels: [Int], $minSalary: Int, $maxSalary: Int, $salaryType: Int, $candidateSalary: Int, $candidateSalaryCurrency: String, $datePosted: Int, $jobTypes: [Int], $workTypes: [String], $industries: [Int], $page: Int, $pageSize: Int, $companyId: String, $advertiserId: String, $userAgent: String, $accNums: Int, $subAccount: Int, $minEdu: Int, $maxEdu: Int, $edus: [Int], $minExp: Int, $maxExp: Int, $seo: String, $searchFields: String, $candidateId: ID, $isDesktop: Boolean, $isCompanySearch: Boolean, $sort: String, $sVi: String, $duplicates: String, $flight: String, $solVisitorId: String) {
  jobs(
    country: $country
    locale: $locale
    keyword: $keyword
    createdAt: $createdAt
    jobFunctions: $jobFunctions
    categories: $categories
    locations: $locations
    careerLevels: $careerLevels
    minSalary: $minSalary
    maxSalary: $maxSalary
    salaryType: $salaryType
    candidateSalary: $candidateSalary
    candidateSalaryCurrency: $candidateSalaryCurrency
    datePosted: $datePosted
    jobTypes: $jobTypes
    workTypes: $workTypes
    industries: $industries
    page: $page
    pageSize: $pageSize
    companyId: $companyId
    advertiserId: $advertiserId
    userAgent: $userAgent
    accNums: $accNums
    subAccount: $subAccount
    minEdu: $minEdu
    edus: $edus
    maxEdu: $maxEdu
    minExp: $minExp
    maxExp: $maxExp
    seo: $seo
    searchFields: $searchFields
    candidateId: $candidateId
    isDesktop: $isDesktop
    isCompanySearch: $isCompanySearch
    sort: $sort
    sVi: $sVi
    duplicates: $duplicates
    flight: $flight
    solVisitorId: $solVisitorId
  ) {
    total
    totalJobs
    relatedSearchKeywords {
      keywords
      type
      totalJobs
    }
    solMetadata
    suggestedEmployer {
      name
      totalJobs
    }
    queryParameters {
      key
      searchFields
      pageSize
    }
    experiments {
      flight
    }
    jobs {
      id
      adType
      sourceCountryCode
      isStandout
      companyMeta {
        id
        advertiserId
        isPrivate
        name
        logoUrl
        slug
      }
      jobTitle
      jobUrl
      jobTitleSlug
      description
      employmentTypes {
        code
        name
      }
      sellingPoints
      locations {
        code
        name
        slug
        children {
          code
          name
          slug
        }
      }
      categories {
        code
        name
        children {
          code
          name
        }
      }
      postingDuration
      postedAt
      salaryRange {
        currency
        max
        min
        period
        term
      }
      salaryVisible
      bannerUrl
      isClassified
      solMetadata
    }
  }
}`

var variables  = {
  "keyword": "",
  "jobFunctions": [
    jobFunction
  ],
  "locations": [],
  "salaryType": 1,
  "jobTypes": [],
  "createdAt": null,
  "careerLevels": [],
  "page": 1,
  "country": country,
  "sVi": "",
  "solVisitorId": "1a9219b7-e948-4d25-bffb-ddbec839ab82",
  "categories": [
    jobFunction+""
  ],
  "workTypes": [
  ],
  "userAgent": "Mozilla/5.0%20(X11;%20Linux%20x86_64)%20AppleWebKit/537.36%20(KHTML,%20like%20Gecko)%20Chrome/103.0.5060.114%20Safari/537.36%20Edg/103.0.1264.51",
  "industries": [],
  "locale": "en"
}


  var result_job =[] 
  var total
  while(true)  {
    try {
    const data = await client.request(query, variables);
    if (variables.page == 1) {
        total = data.jobs.total
    }

    if (data.jobs.jobs.length == 0) {
        break;
    }else{
        variables.page += 1;
        data.jobs.jobs.forEach(async data => {
          const result = await getJobsDetails(data.id)
          try {
          data.getJobsDetails = result;
          result_job.push(data);
          } catch (error) {
            console.log("Job Id :",data.id);
            console.log("Result Data :",result);
          }
        });
        console.log(`Result Data : ${result_job.length}/${total}`);
    }
    }catch (error) {
      console.log(error);
    }
  }

  fs.writeFileSync(`./jobstreet_sg/result_data/${Date.now()}_raw_${country}.json`,JSON.stringify(result_job, null, 2))
  console.log("Successful Export Data");