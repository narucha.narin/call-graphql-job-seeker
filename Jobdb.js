import { gql,GraphQLClient } from 'graphql-request'
import fs from 'fs'

const endpoint =`https://xapi.supercharge-srp.co/job-search/graphql?country=th&isSmartSearch=false`;
const client = new GraphQLClient(endpoint)

const query = gql`
query getJobs($country: String, $locale: String, $keyword: String, $createdAt: String, $jobFunctions: [Int], $categories: [String], $locations: [Int], $careerLevels: [Int], $minSalary: Int, $maxSalary: Int, $salaryType: Int, $candidateSalary: Int, $candidateSalaryCurrency: String, $datePosted: Int, $jobTypes: [Int], $workTypes: [String], $industries: [Int], $page: Int, $pageSize: Int, $companyId: String, $advertiserId: String, $userAgent: String, $accNums: Int, $subAccount: Int, $minEdu: Int, $maxEdu: Int, $edus: [Int], $minExp: Int, $maxExp: Int, $seo: String, $searchFields: String, $candidateId: ID, $isDesktop: Boolean, $isCompanySearch: Boolean, $sort: String, $sVi: String, $duplicates: String, $flight: String, $solVisitorId: String) {
    jobs(
      country: $country
      locale: $locale
      keyword: $keyword
      createdAt: $createdAt
      jobFunctions: $jobFunctions
      categories: $categories
      locations: $locations
      careerLevels: $careerLevels
      minSalary: $minSalary
      maxSalary: $maxSalary
      salaryType: $salaryType
      candidateSalary: $candidateSalary
      candidateSalaryCurrency: $candidateSalaryCurrency
      datePosted: $datePosted
      jobTypes: $jobTypes
      workTypes: $workTypes
      industries: $industries
      page: $page
      pageSize: $pageSize
      companyId: $companyId
      advertiserId: $advertiserId
      userAgent: $userAgent
      accNums: $accNums
      subAccount: $subAccount
      minEdu: $minEdu
      edus: $edus
      maxEdu: $maxEdu
      minExp: $minExp
      maxExp: $maxExp
      seo: $seo
      searchFields: $searchFields
      candidateId: $candidateId
      isDesktop: $isDesktop
      isCompanySearch: $isCompanySearch
      sort: $sort
      sVi: $sVi
      duplicates: $duplicates
      flight: $flight
      solVisitorId: $solVisitorId
    ) {
      total
      totalJobs
      relatedSearchKeywords {
        keywords
        type
        totalJobs
      }
      solMetadata
      suggestedEmployer {
        name
        totalJobs
      }
      queryParameters {
        key
        searchFields
        pageSize
      }
      experiments {
        flight
      }
      jobs {
        id
        sellingPoints
      }
    }
  }
`

var variables  = {
  "keyword": "",
  "jobFunctions": [
      131
  ],
  "locations": [],
  "salaryType": 1,
  "jobTypes": [],
  "createdAt": null,
  "careerLevels": [],
  "page": 1,
  "country": "th",
  "sVi": "",
  "solVisitorId": "881ff989-f51c-4ba5-beb3-f9567cb3381d",
  "categories": [
      "131"
  ],
  "workTypes": [],
  "userAgent": "Mozilla/5.0%20(Windows%20NT%2010.0;%20Win64;%20x64)%20AppleWebKit/537.36%20(KHTML,%20like%20Gecko)%20Chrome/107.0.0.0%20Safari/537.36%20Edg/107.0.1418.42",
  "industries": [],
  "locale": "en"
}

const getJobsDetails = async (id) => {
  const query = `query getJobDetail($jobId: String, $locale: String, $country: String, $candidateId: ID, $solVisitorId: String, $flight: String) {
    jobDetail(
      jobId: $jobId
      locale: $locale
      country: $country
      candidateId: $candidateId
      solVisitorId: $solVisitorId
      flight: $flight
    ) {
      header {
        jobTitle
        company {
          name
        }
      }
      jobDetail {
        summary
        jobDescription {
          html
        }
        jobRequirement {
          careerLevel
          yearsOfExperience
          qualification
          fieldOfStudy
          industryValue {
            value
            label
          }
          skills
          employmentType
          languages
          postedDate
          closingDate
          jobFunctionValue {
            name
          }
        }
      }
    }
  }`

const variables = {
  "jobId": id,
  "country": "th",
  "locale": "en",
  "candidateId": "",
  "solVisitorId": "881ff989-f51c-4ba5-beb3-f9567cb3381d"
}
  const data = await client.request(query, variables);
  return data;
}

  var result_job =[] 
  var total
  while(true)  {
    const data = await client.request(query, variables);
    if (variables.page == 1) {
        total = data.jobs.total
    }

    if (data.jobs.jobs.length == 0) {
        break;
    }else{
        variables.page += 1;
        data.jobs.jobs.forEach(async data => {
          const objTemplate = {
            companyName: "",
            jobTitle: "",
            jobSummary: [],
            jobDescription: "",
            sellingPoints: data.sellingPoints,
            jobFunction: []
          }
          const result = await getJobsDetails(data.id)
          objTemplate.jobTitle = result.jobDetail.header.jobTitle
          objTemplate.companyName = result.jobDetail.header.company.name
          objTemplate.jobSummary = result.jobDetail.jobDetail.summary
          objTemplate.jobDescription = result.jobDetail.jobDetail.jobDescription.html
          objTemplate.jobFunction = result.jobDetail.jobDetail.jobRequirement.jobFunctionValue.map(row => {
            return row.name
          })
          result_job.push(objTemplate);
        });
        console.log(`Result Data : ${result_job.length}/${total}`);
    }
  }

  fs.writeFileSync(`./result_data/${Date.now()}_${variables.keyword}.json`,JSON.stringify(result_job, null, 2))
  console.log("Successful Export Data");